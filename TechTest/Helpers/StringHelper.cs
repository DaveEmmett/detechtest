﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechTest.Helpers
{
    public static class StringHelper
    {
        public static string Reverse(this string input)
        {
            char[] arr = input.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static string RemoveWhitespace(this string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

        public static bool IsPalindrome(this string input)
        { 
            var whitespaceLess = RemoveWhitespace(input).ToLower();
            return whitespaceLess == Reverse(whitespaceLess);
        }
    }
}