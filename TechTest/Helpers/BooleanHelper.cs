﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechTest.Helpers
{
    public static class BooleanHelper
    {
        public static string ToYesNoString(this bool value)
        {
            return value ? "Yes" : "No";
        }
    }
}