
$(function () {
    function loadTable() {
        var holder = $(".js-table-holder");

        $.ajax({
            type: "GET",
            url: holder.data('action'),
        }).done(function (response) {
            holder.html(response);
        });;
    }
    function datepicker() {
    }

    $('body').on('click', '.js-modal', function (e) {
        e.preventDefault();
        var link = $(this);
        var modal = $("#" + link.data("modal"))
        var modalBody = modal.find(".modal-body")
        var modalTitle = modal.find(".modal-title")

        $.ajax({
            type: "GET",
            url: link.attr('href'),
        }).done(function (response) {
            modalTitle.html(link.attr("title"));
            modalBody.html(response);
            datepicker();
            modal.modal('show');
        });;

        return false;
    });

    $('body').on('submit', '.js-ajax-form', function (e) {
        e.preventDefault;
        var form = $(this);

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (response) {
            if (response.success) {
                form.closest(".modal").modal('hide');
                loadTable();
            } else {
                form.closest(".modal-body").html(response);
                datepicker();
            }
        });;

        return false;
    });
});