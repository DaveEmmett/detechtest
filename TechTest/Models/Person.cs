﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TechTest.Models
{
    public class Person : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateActive { get; set; }
        public bool IsWebCustomer { get; set; }
        public bool PreviouslyOrdered { get; set; }
        public ICollection<Colour> Colours { get; set; }

        public int PersonId { get; set; }

        public string FullName()
        {
            return string.Format("{0} {1}", FirstName ?? "", LastName ?? "");
        }

        public ICollection<Colour> GetColours()
        {
            return Colours ?? new Collection<Colour>();
        }
    }
}