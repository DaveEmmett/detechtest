﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechTest.Models
{
    public class Colour : Entity
    {
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public ICollection<Person> People { get; set; }

        public int ColourId { get; set; }
    }
}