﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechTest.Models
{
    public class CheckListItem
    {
        public Guid Id { get; set; }
        public string Display { get; set; }
        public bool Checked { get; set; }
    }
}