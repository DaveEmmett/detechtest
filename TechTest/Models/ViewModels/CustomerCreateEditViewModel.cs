﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TechTest.Models.ViewModels
{
    public class CustomerCreateEditViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [DisplayName("First Name*")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        public bool PreviouslyOrdered { get; set; }
        public bool IsWebCustomer { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateActive { get; set; }
        public List<CheckListItem> ColourList { get; set; }
    }
}