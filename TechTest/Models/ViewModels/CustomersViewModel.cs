﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechTest.Models.ViewModels
{
    public class CustomersViewModel : Entity
    {
        public IList<Person> People { get; set; }
        public int Total { get; set; }
        public int TotalPages { get; set; }
        public int PageBy { get; set; }
        public int? Page { get; set; }
    }
}