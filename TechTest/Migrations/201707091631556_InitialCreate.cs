namespace TechTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Colours",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"),
                    Name = c.String(maxLength: 4000),
                        IsEnabled = c.Boolean(nullable: false),
                        ColourId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"),
                    FirstName = c.String(maxLength: 4000),
                        LastName = c.String(maxLength: 4000),
                        DateActive = c.DateTime(nullable: false),
                        IsWebCustomer = c.Boolean(nullable: false),
                        PreviouslyOrdered = c.Boolean(nullable: false),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);            
        }
        
        public override void Down()
        {
            DropTable("dbo.People");
            DropTable("dbo.Colours");
        }
    }
}
