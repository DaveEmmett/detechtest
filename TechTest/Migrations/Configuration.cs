using CsvHelper;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using TechTest.Data;
using TechTest.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TechTest.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<TechTest.Data.TechTestDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }
        
        protected override void Seed(TechTestDbContext context)
        {
            var assembly = Assembly.GetExecutingAssembly();
            string resourceName = "TechTest.Data.SeedData.colours.csv";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var colours = csvReader.GetRecords<Colour>().ToArray();
                    context.Colours.AddOrUpdate(x => x.ColourId, colours);
                    context.SaveChanges();
                }
            }

            resourceName = "TechTest.Data.SeedData.people.csv";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var people = csvReader.GetRecords<Person>().ToArray();
                    context.People.AddOrUpdate(x => x.PersonId, people);
                    context.SaveChanges();
                }
            }

            resourceName = "TechTest.Data.SeedData.favourite_colours.csv";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var favouriteColours = csvReader.GetRecords<dynamic>().ToArray();
                    foreach (var favouriteColour in favouriteColours)
                    {
                        int personId = Int32.Parse(favouriteColour.PersonId);
                        int colourId = Int32.Parse(favouriteColour.ColourId);
                        var person = context.People.First(x => x.PersonId == personId);
                        var colour = context.Colours.First(x => x.ColourId == colourId);
                        if(person.Colours == null) person.Colours = new Collection<Colour>();
                        if (!person.GetColours().Contains(colour)) person.Colours.Add(colour);                        
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}
