﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TechTest.Data;
using TechTest.Models;
using TechTest.Models.ViewModels;

namespace TechTest.Controllers
{
    public class CustomersController : Controller
    {
        private readonly TechTestDbContext dbContext;

        public CustomersController(TechTestDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public ActionResult Index(int? page)
        {
            var skip = 0;
            var take = 20;
            var totalPages = 0;

            if (page.HasValue && page.Value > 1)
            {
                skip = take * (page.Value - 1);
            }

            var people = dbContext.People
               .Include("Colours")
               .OrderByDescending(x => x.DateActive).Skip(skip).Take(take);

            if (dbContext.People.Any())
            {
                totalPages = (dbContext.People.Count() + take - 1) / take;
            };

            var model = new CustomersViewModel()
            {
                People = people.ToList(),
                Total = dbContext.People.Count(),
                PageBy = take,
                Page = page,
                TotalPages = totalPages
            };

            if (Request.IsAjaxRequest())
            {
                return PartialView("_table", model);
            }

            return View(model);
        }

        private List<CheckListItem> GetColourList(Person person)
        {
            return dbContext.Colours.ToList().Select(x =>
                new CheckListItem()
                {
                    Id = x.Id,
                    Display = x.Name,
                    Checked = person.Colours.Contains(x)
                }
            ).ToList();
        }

        private List<CheckListItem> GetColourList()
        {
            return dbContext.Colours.ToList().Select(x =>
                new CheckListItem()
                {
                    Id = x.Id,
                    Display = x.Name,
                    Checked = false
                }
            ).ToList();
        }

        [HttpGet]
        public ActionResult Update(Guid id)
        {
            var person = dbContext.People
                .Include("Colours")
                .SingleOrDefault(x => x.Id == id);

            var colourList = GetColourList(person);

            return PartialView(new CustomerCreateEditViewModel()
            {
                Id = person.Id,
                FirstName = person.FirstName,
                //LastName = person.LastName,
                PreviouslyOrdered = person.PreviouslyOrdered,
                IsWebCustomer = person.IsWebCustomer,
                DateActive = person.DateActive,
                ColourList = colourList
            });
        }

        [HttpPost]
        public ActionResult Update(CustomerCreateEditViewModel model)
        {
            var person = dbContext.People
                .Include("Colours")
                .SingleOrDefault(x => x.Id == model.Id);

            if (ModelState.IsValid)
            {
                var selectedColourIds = model.ColourList.Where(x => x.Checked).Select(x => x.Id);

                person.FirstName = model.FirstName;
                //person.LastName = model.LastName;
                person.PreviouslyOrdered = model.PreviouslyOrdered;
                person.IsWebCustomer = model.IsWebCustomer;
                person.DateActive = model.DateActive;
                person.Colours = dbContext.Colours.Where(x => selectedColourIds.Contains(x.Id)).ToList();

                dbContext.SaveChanges();
                return Json(new { success = true });
            }

            model.ColourList = GetColourList(person);

            return PartialView(model);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            var colourList = GetColourList();

            return PartialView(new CustomerCreateEditViewModel()
            {
                DateActive = DateTime.Now,
                ColourList = colourList
            });
        }

        [HttpPost]
        public ActionResult Insert(CustomerCreateEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var selectedColourIds = model.ColourList.Where(x => x.Checked).Select(x => x.Id);

                var person = new Person();
                person.FirstName = model.FirstName;
                person.LastName = model.LastName;
                person.PreviouslyOrdered = model.PreviouslyOrdered;
                person.IsWebCustomer = model.IsWebCustomer;
                person.DateActive = model.DateActive;
                person.Colours = dbContext.Colours.Where(x => selectedColourIds.Contains(x.Id)).ToList();

                dbContext.People.Add(person);
                dbContext.SaveChanges();

                return Json(new { success = true });
            }

            model.ColourList = GetColourList();

            return PartialView(model);
        }
    }
}