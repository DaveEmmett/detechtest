﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TechTest.Models;
using TechTest.Migrations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechTest.Data
{
    public class TechTestDbContext : DbContext
    {
        public TechTestDbContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<TechTestDbContext, Configuration>());
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Colour> Colours { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder
                .Properties()
                .Where(x => x.PropertyType == typeof(Guid) && x.Name.EndsWith("Id"))
                .Configure(x => x.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity));

            modelBuilder
                .Entity<Person>()
                .HasMany<Colour>(x => x.Colours)
                .WithMany(x => x.People)
                .Map(x =>
                {
                    x.MapLeftKey("PersonId");
                    x.MapRightKey("ColourId");
                    x.ToTable("FavouriteColours");
                });
        }
    }
}